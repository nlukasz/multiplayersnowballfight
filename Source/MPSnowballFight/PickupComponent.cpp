// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupComponent.h"
#include "UsablePickup.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UPickupComponent::UPickupComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UPickupComponent::BeginPlay()
{
	Super::BeginPlay();
	StartPickupLifeTimeTimer();
}

void UPickupComponent::UsePickup(UObject* InContextObject)
{
	if ((PickupData != nullptr) && (PickupData->GetClass()->ImplementsInterface(UUsablePickup::StaticClass())))
	{
		IUsablePickup::Execute_UsePickup(PickupData, InContextObject);
	}
	GetOwner()->Destroy();
}

void UPickupComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if (PickupLifeTimeTimerHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(PickupLifeTimeTimerHandle);
	}
}

void UPickupComponent::StartPickupLifeTimeTimer()
{
	float timerDuration = FMath::FRandRange(PickupLifeTimeDurationRange.X, PickupLifeTimeDurationRange.Y);
	if (timerDuration > 0.0f)
	{
		GetWorld()->GetTimerManager().SetTimer(PickupLifeTimeTimerHandle, this, &UPickupComponent::HandleLifeTimeTimerFinished, timerDuration);
	}
}

void UPickupComponent::HandleLifeTimeTimerFinished()
{
	GetOwner()->Destroy();
}
