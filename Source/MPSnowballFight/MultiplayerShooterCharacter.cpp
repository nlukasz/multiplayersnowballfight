// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerShooterCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "Bullet.h"
#include "PickupComponent.h"

// Sets default values
AMultiplayerShooterCharacter::AMultiplayerShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(GetCapsuleComponent());
	Camera->SetRelativeLocation(FVector(20.0f, 0.0f, 30.0f));
	Camera->bUsePawnControlRotation = true;

	GunAttachPoint = CreateDefaultSubobject<USceneComponent>("GunAttachPoint");
	GunAttachPoint->SetupAttachment(Camera);
	GunAttachPoint->SetRelativeLocation(FVector(30.0f, 0.0f, -30.0f));
}

void AMultiplayerShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	// Below is workaround for replicating camera's pitch rotation for server and other clients. This is not the best solution, as I'm sending data every time camera is updated. Having it in timer instead tick would improve network performance, but I would prefer to replace it with proper solution once I understand how Pawn and PlayerController are replicating data.
	if (IsLocallyControlled())
	{
		FRotator pawnViewRotation = GetViewRotation();
		if (!PawnControlRotation.Equals(pawnViewRotation))
		{
			PawnControlRotation = pawnViewRotation;
			UpdatePawnControlRotation_Server(PawnControlRotation);
		}
	}
}

// Called when the game starts or when spawned
void AMultiplayerShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (GetLocalRole() == ROLE_Authority)
	{
		SpawnAndInitializeGun();
		GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AMultiplayerShooterCharacter::HandleCapsuleHit);
		GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AMultiplayerShooterCharacter::HandleCapsuleBeginOverlap);
	}
}

void AMultiplayerShooterCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if ((GetLocalRole() == ROLE_Authority) && (SpawnedGun != nullptr))
	{
		SpawnedGun->Destroy();
		SpawnedGun = nullptr;
	}
}

void AMultiplayerShooterCharacter::HandleCapsuleHit(UPrimitiveComponent* InHitComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, FVector InNormalImpulse, const FHitResult& InHit)
{
	if (!IsAlive())
	{
		return;
	}

	const ABullet* bulletActor = Cast <ABullet>(InOtherActor);
	if (bulletActor != nullptr)
	{
		AddToHealth(bulletActor->GetDamage() * (-1.0f));
	}
}

void AMultiplayerShooterCharacter::HandleCapsuleBeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, int32 InOtherBodyIndex, bool InFromSweep, const FHitResult& InSweepResult)
{
	if (!IsAlive())
	{
		return;
	}

	UPickupComponent* pickupComponent = InOtherActor->GetComponentByClass<UPickupComponent>();
	if (pickupComponent != nullptr)
	{
		pickupComponent->UsePickup(this);
	}
}

void AMultiplayerShooterCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMultiplayerShooterCharacter, CurrentHealth);
	DOREPLIFETIME(AMultiplayerShooterCharacter, SpawnedGun);
	DOREPLIFETIME_CONDITION(AMultiplayerShooterCharacter, PawnControlRotation, COND_SkipOwner);
}

bool AMultiplayerShooterCharacter::IsAlive() const
{
	return (CurrentHealth > 0.0f);
}

float AMultiplayerShooterCharacter::GetCurrentHealth() const
{
	return CurrentHealth;
}

AGun* AMultiplayerShooterCharacter::GetGun() const
{
	return SpawnedGun;
}

void AMultiplayerShooterCharacter::IncreaseHealthBy(const float InExtraHealth)
{
	if ((GetLocalRole() == ROLE_Authority) && (InExtraHealth > 0.0f))
	{
		AddToHealth(InExtraHealth);
	}
}

void AMultiplayerShooterCharacter::RequestGunShot()
{
	if (IsAlive() && (SpawnedGun != nullptr) && SpawnedGun->IsReadyToShoot())
	{
		RequestGunShot_Server();
	}
}

void AMultiplayerShooterCharacter::RequestGunShot_Server_Implementation()
{
	if (SpawnedGun != nullptr)
	{
		SpawnedGun->TryShooting();
	}
}

void AMultiplayerShooterCharacter::SpawnAndInitializeGun()
{
	if ((GunClass == nullptr) || (SpawnedGun != nullptr))
	{
		return;
	}

	FActorSpawnParameters gunSpawnParameters;
	gunSpawnParameters.Owner = this;
	gunSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	SpawnedGun = GetWorld()->SpawnActor<AGun>(GunClass, GunAttachPoint->GetComponentLocation(), GunAttachPoint->GetComponentRotation(), gunSpawnParameters);
	OnRep_SpawnedGun();
	if (SpawnedGun != nullptr)
	{
		SpawnedGun->AttachToComponent(GunAttachPoint, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false));
	}
}

void AMultiplayerShooterCharacter::AddToHealth(const float InAddition)
{
	CurrentHealth = FMath::Clamp(CurrentHealth + InAddition, 0.0f, MaxHealth);
	OnRep_CurrentHealth();
	if (FMath::IsNearlyEqual(CurrentHealth, 0.0f))
	{
		OnPlayerDefeated.Broadcast();
	}
}

void AMultiplayerShooterCharacter::UpdatePawnControlRotation_Server_Implementation(const FRotator InNewPawnControlRotation)
{
	PawnControlRotation = InNewPawnControlRotation;
	OnRep_PawnControlRotation();
}

void AMultiplayerShooterCharacter::OnRep_CurrentHealth()
{
	OnPlayerHealthChanged.Broadcast(CurrentHealth);
}

void AMultiplayerShooterCharacter::OnRep_SpawnedGun()
{
	OnChangedGun.Broadcast(SpawnedGun);
}

void AMultiplayerShooterCharacter::OnRep_PawnControlRotation()
{
	Camera->SetWorldRotation(PawnControlRotation);
}