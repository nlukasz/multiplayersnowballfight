// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "Engine/World.h"
#include "TimerManager.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
	SetReplicateMovement(true);

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>("BulletMesh");
	SetRootComponent(BulletMesh);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovement");
	ProjectileMovement->bRotationFollowsVelocity = true;
}

float ABullet::GetDamage() const
{
	// TODO: calculate from projectile movement
	return BaseDamage;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetLocalRole() == ROLE_Authority)
	{
		if (MaxLifeTime > 0.0f)
		{
			GetWorld()->GetTimerManager().SetTimer(MaxLifeTimeTimerHandle, this, &ABullet::HandleMaxLifeTimeTimerFinished, MaxLifeTime);
		}
		ProjectileMovement->OnProjectileStop.AddDynamic(this, &ABullet::HandleProjectileStopped);
	}
}

void ABullet::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if (MaxLifeTimeTimerHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(MaxLifeTimeTimerHandle);
	}
}

void ABullet::HandleMaxLifeTimeTimerFinished()
{
	Destroy();
}

void ABullet::HandleProjectileStopped(const FHitResult& InImpactResult)
{
	Destroy();
}

