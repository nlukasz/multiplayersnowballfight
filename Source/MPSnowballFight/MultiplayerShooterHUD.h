// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Blueprint/UserWidget.h"
#include "MultiplayerShooterHUDWidget.h"
#include "MultiplayerShooterCharacter.h"
#include "Gun.h"
#include "MultiplayerShooterHUD.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class MPSNOWBALLFIGHT_API AMultiplayerShooterHUD : public AHUD
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooter HUD")
	TSubclassOf<UUserWidget> ShooterHUDWidgetClass = nullptr;

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	void UpdatePlayerGunAmmoCountVisibility(const bool InVisible);
	void UpdatePawn(APawn* InNewPawn);

	UFUNCTION()
	void HandlePossessedPawnChanged(APawn* InOldPawn, APawn* InNewPawn);

	UFUNCTION()
	void HandlePlayerHealthChanged(const float InCurrentHealth);

	UFUNCTION()
	void HandlePlayerChangedGun(AGun* InNewGun);

	UFUNCTION()
	void HandleAmmoCountChanged(const int32 InCurrentAmmoCount);

	UUserWidget* ShooterHUDWidget = nullptr;
	AMultiplayerShooterCharacter* ShooterCharacter = nullptr;
	AGun* CurrentPlayerGun = nullptr;
};
