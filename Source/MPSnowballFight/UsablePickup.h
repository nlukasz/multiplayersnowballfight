// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UsablePickup.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UUsablePickup : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MPSNOWBALLFIGHT_API IUsablePickup
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent)
	void UsePickup(UObject* InContextObject);
};
