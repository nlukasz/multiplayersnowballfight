// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MultiplayerShooterHUDWidget.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UMultiplayerShooterHUDWidget : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MPSNOWBALLFIGHT_API IMultiplayerShooterHUDWidget
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void UpdatePlayerHealth(const float InCurrentPlayerHealth);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void UpdatePlayerGunAmmoCountVisibility(const bool InVisible);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void UpdatePlayerGunAmmoCount(const int32 InCurrentAmmoCount);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void UpdatePlayerScore(const int32 InCurrentPlayerScore);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetIsSpectatorModeActive(const float InSpectatorModeActive);
};
