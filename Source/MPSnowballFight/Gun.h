// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Gun.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAmmoCountChanged, const int32, InCurrentAmmoCount);

UCLASS(Blueprintable, BlueprintType)
class MPSNOWBALLFIGHT_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gun")
	bool IsReadyToShoot() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Gun")
	int32 GetCurrentAmmoCount() const;

	UFUNCTION(BlueprintCallable, Category = "Gun")
	void TryShooting();

	UFUNCTION(BlueprintCallable, Category = "Gun")
	void TryAddingAmmo(const int32 InNewBulletsCount);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Gun", Meta = (ClampMin = "0.0"))
	float CooldownDuration = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Gun")
	TSubclassOf<AActor> BulletClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gun", Meta = (ClampMin = "0"))
	int32 InitialAmmoCount = 5;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Gun", Meta = (ClampMin = "0"))
	int32 MaxAmmoCount = 5;

	UPROPERTY(BlueprintAssignable, Category = "Gun")
	FAmmoCountChanged OnAmmoCountChanged;

protected:
	// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* GunMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UArrowComponent* GunBarrelArrow = nullptr;

private:
	void UpdateCurrentAmmoCount(const int32 InNewAmmoCount);

	UFUNCTION()
	void ResetCooldown();

	UFUNCTION()
	void OnRep_CurrentAmmoCount();
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_CurrentAmmoCount)
	int32 CurrentAmmoCount = 0;

	UPROPERTY(Replicated)
	bool CooldownActive = false;
};
