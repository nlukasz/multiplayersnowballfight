// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/SceneComponent.h"
#include "Camera/CameraComponent.h"
#include "Gun.h"
#include "MultiplayerShooterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDefeated);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerHealthChanged, const float, InCurrentHealth);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FChangedGun, AGun*, InNewGun);

UCLASS(Blueprintable, BlueprintType)
class MPSNOWBALLFIGHT_API AMultiplayerShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMultiplayerShooterCharacter();

	virtual void Tick(float DeltaSeconds) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* GunAttachPoint = nullptr;

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Shooter Character")
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Shooter Character")
	float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Shooter Character")
	AGun* GetGun() const;

	UFUNCTION(BlueprintCallable, Category = "Shooter Character")
	void IncreaseHealthBy(const float InExtraHealth);

	UFUNCTION(BlueprintCallable, Category = "Shooter Character")
	void RequestGunShot();

	UFUNCTION(Server, Reliable)
	void RequestGunShot_Server();
	void RequestGunShot_Server_Implementation();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Shooter Character|Settings", Meta = (ClampMin = "0.0"))
	float MaxHealth = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shooter Character|Settings")
	TSubclassOf<AGun> GunClass = nullptr;

	UPROPERTY(BlueprintAssignable, Category = "Shooter Character")
	FPlayerDefeated OnPlayerDefeated;

	UPROPERTY(BlueprintAssignable, Category = "Shooter Character")
	FPlayerHealthChanged OnPlayerHealthChanged;

	UPROPERTY(BlueprintAssignable, Category = "Shooter Character")
	FChangedGun OnChangedGun;

private:
	void SpawnAndInitializeGun();
	void AddToHealth(const float InAddition);

	UFUNCTION()
	void HandleCapsuleHit(UPrimitiveComponent* InHitComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, FVector InNormalImpulse, const FHitResult& InHit);

	UFUNCTION()
	void HandleCapsuleBeginOverlap(UPrimitiveComponent* InOverlappedComponent, AActor* InOtherActor, UPrimitiveComponent* InOtherComp, int32 InOtherBodyIndex, bool InFromSweep, const FHitResult& InSweepResult);

	UFUNCTION(Server, Unreliable)
	void UpdatePawnControlRotation_Server(const FRotator InNewPawnControlRotation);
	void UpdatePawnControlRotation_Server_Implementation(const FRotator InNewPawnControlRotation);

	UFUNCTION()
	void OnRep_CurrentHealth();
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_CurrentHealth)
	float CurrentHealth = MaxHealth;

	UFUNCTION()
	void OnRep_SpawnedGun();
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_SpawnedGun)
	AGun* SpawnedGun = nullptr;

	UFUNCTION()
	void OnRep_PawnControlRotation();
	UPROPERTY(Replicated, ReplicatedUsing = OnRep_PawnControlRotation)
	FRotator PawnControlRotation = FRotator::ZeroRotator;
};
