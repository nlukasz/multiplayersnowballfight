// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/TimerHandle.h"
#include "Bullet.generated.h"

UCLASS(Blueprintable, BlueprintType)
class MPSNOWBALLFIGHT_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Bullet")
	float GetDamage() const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Bullet", Meta = (ClampMin = "0.0"))
	float MaxLifeTime = 15.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Bullet", Meta = (ClampMin = "0.0"))
	float BaseDamage = 20.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* BulletMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UProjectileMovementComponent* ProjectileMovement = nullptr;

private:
	UFUNCTION()
	void HandleMaxLifeTimeTimerFinished();

	UFUNCTION()
	void HandleProjectileStopped(const FHitResult& InImpactResult);

	FTimerHandle MaxLifeTimeTimerHandle;
};
