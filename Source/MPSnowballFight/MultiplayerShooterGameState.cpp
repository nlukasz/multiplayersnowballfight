// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerShooterGameState.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

void AMultiplayerShooterGameState::BeginPlay()
{
	Super::BeginPlay();

	if (GetLocalRole() == ROLE_Authority)
	{
		PickupActorClassesAsArray = PickupActorClasses.Array();
		PickupActorClassesAsArray.Remove(nullptr);

		UGameplayStatics::GetAllActorsWithTag(GetWorld(), PickupSpawnLocationActorTag, AvailablePickupSpawnLocations);

		if ((PickupActorClassesAsArray.Num() > 0) && (AvailablePickupSpawnLocations.Num() > 0))
		{
			StartNextPickupSpawnTimer();
		}
	}
}

void AMultiplayerShooterGameState::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (NextPickupSpawnTimerHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(NextPickupSpawnTimerHandle);
	}
}

void AMultiplayerShooterGameState::StartNextPickupSpawnTimer()
{
	float timerDuration = FMath::FRandRange(NextPickupSpawnTimeRange.X, NextPickupSpawnTimeRange.Y);
	if (timerDuration > 0.0f)
	{
		GetWorld()->GetTimerManager().SetTimer(NextPickupSpawnTimerHandle, this, &AMultiplayerShooterGameState::HandleNextPickupSpawnTimerFinished, timerDuration);
	}
}

void AMultiplayerShooterGameState::SpawnNewPickup()
{
	TSubclassOf<AActor> pickupToSpawnClass = PickupActorClassesAsArray[FMath::RandRange(0, PickupActorClassesAsArray.Num() - 1)];
	const int32 indexOfNewPickupLocation = FMath::RandRange(0, AvailablePickupSpawnLocations.Num() - 1);
	AActor* newPickupLocation = AvailablePickupSpawnLocations[indexOfNewPickupLocation];

	FActorSpawnParameters pickupSpawnParameters;
	pickupSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	AActor* spawnedPickup = GetWorld()->SpawnActor<AActor>(pickupToSpawnClass, newPickupLocation->GetActorLocation(), newPickupLocation->GetActorRotation(), pickupSpawnParameters);

	if (spawnedPickup != nullptr)
	{
		SpawnedPickups.Add(spawnedPickup, newPickupLocation);
		AvailablePickupSpawnLocations.RemoveAt(indexOfNewPickupLocation);
		spawnedPickup->OnDestroyed.AddDynamic(this, &AMultiplayerShooterGameState::HandlePickupActorDestroyed);
	}
}

void AMultiplayerShooterGameState::HandleNextPickupSpawnTimerFinished()
{
	SpawnNewPickup();
	StartNextPickupSpawnTimer();
}

void AMultiplayerShooterGameState::HandlePickupActorDestroyed(AActor* InDestroyedActor)
{
	AActor* pickupLocationActor = SpawnedPickups.FindChecked(InDestroyedActor);
	AvailablePickupSpawnLocations.Add(pickupLocationActor);
	SpawnedPickups.Remove(InDestroyedActor);
}
