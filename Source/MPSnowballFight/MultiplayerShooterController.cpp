// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerShooterController.h"
#include "Engine/LocalPlayer.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "MultiplayerShooterCharacter.h"
#include "GameFramework/GameModeBase.h"

void AMultiplayerShooterController::BeginPlay()
{
	Super::BeginPlay();
	if (IsLocalController() && (InputMappingContext != nullptr))
	{
		UEnhancedInputLocalPlayerSubsystem* enhancedInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());
		if (enhancedInputSubsystem != nullptr)
		{
			enhancedInputSubsystem->AddMappingContext(InputMappingContext, 0);
		}
	}
}

void AMultiplayerShooterController::SetupInputComponent()
{
	Super::SetupInputComponent();
	UEnhancedInputComponent* enhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent);
	if (enhancedInputComponent != nullptr)
	{
		if (ShootInputAction != nullptr)
		{
			enhancedInputComponent->BindAction(ShootInputAction, ETriggerEvent::Triggered, this, &AMultiplayerShooterController::ShootFromCharacterGun);
		}
		if (MovementInputAction != nullptr)
		{
			enhancedInputComponent->BindAction(MovementInputAction, ETriggerEvent::Triggered, this, &AMultiplayerShooterController::MoveCharacter);
		}
		if (CameraLookInputAction)
		{
			enhancedInputComponent->BindAction(CameraLookInputAction, ETriggerEvent::Triggered, this, &AMultiplayerShooterController::RotateCharacter);
		}
	}
}

void AMultiplayerShooterController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (GetLocalRole() != ROLE_Authority)
	{
		return;
	}

	AMultiplayerShooterCharacter* shooterCharacter = Cast<AMultiplayerShooterCharacter>(InPawn);
	if (shooterCharacter != nullptr)
	{
		shooterCharacter->OnPlayerDefeated.AddDynamic(this, &AMultiplayerShooterController::HandlePlayerDefeated);
	}
}

void AMultiplayerShooterController::MoveCharacter(const FInputActionValue& InValue)
{
	AMultiplayerShooterCharacter* shooterCharacter = Cast<AMultiplayerShooterCharacter>(GetPawn());
	if ((shooterCharacter != nullptr) && shooterCharacter->IsAlive())
	{
		const FVector2D inputVector = InValue.Get<FVector2D>();
		shooterCharacter->AddMovementInput(shooterCharacter->GetActorForwardVector(), inputVector.Y);
		shooterCharacter->AddMovementInput(shooterCharacter->GetActorRightVector(), inputVector.X);
	}
}

void AMultiplayerShooterController::RotateCharacter(const FInputActionValue& InValue)
{
	AMultiplayerShooterCharacter* shooterCharacter = Cast<AMultiplayerShooterCharacter>(GetPawn());
	if ((shooterCharacter != nullptr) && shooterCharacter->IsAlive())
	{
		const FVector2D inputVector = InValue.Get<FVector2D>();
		shooterCharacter->AddControllerYawInput(inputVector.X);
		shooterCharacter->AddControllerPitchInput(inputVector.Y);
	}
}

void AMultiplayerShooterController::ShootFromCharacterGun()
{
	AMultiplayerShooterCharacter* shooterCharacter = Cast<AMultiplayerShooterCharacter>(GetPawn());
	if ((shooterCharacter != nullptr) && shooterCharacter->IsAlive())
	{
		shooterCharacter->RequestGunShot();
	}
}

void AMultiplayerShooterController::HandlePlayerDefeated()
{
	APawn* controlledPawn = GetPawn();
	controlledPawn->Destroy();
	AGameModeBase* gameMode = GetWorld()->GetAuthGameMode();
	gameMode->RestartPlayer(this);
}
