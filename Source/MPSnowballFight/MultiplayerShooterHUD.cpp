// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerShooterHUD.h"

void AMultiplayerShooterHUD::BeginPlay()
{
	Super::BeginPlay();

	if ((ShooterHUDWidgetClass != nullptr) && ShooterHUDWidgetClass->ImplementsInterface(UMultiplayerShooterHUDWidget::StaticClass()))
	{
		ShooterHUDWidget = CreateWidget<UUserWidget>(PlayerOwner, ShooterHUDWidgetClass);
		ShooterHUDWidget->AddToViewport();
	}

	UpdatePawn(PlayerOwner->GetPawn());
	PlayerOwner->OnPossessedPawnChanged.AddDynamic(this, &AMultiplayerShooterHUD::HandlePossessedPawnChanged);
}

void AMultiplayerShooterHUD::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if (ShooterHUDWidget != nullptr)
	{
		ShooterHUDWidget->RemoveFromParent();
		ShooterHUDWidget = nullptr;
	}
}

void AMultiplayerShooterHUD::UpdatePlayerGunAmmoCountVisibility(const bool InVisible)
{
	if (ShooterHUDWidget != nullptr)
	{
		IMultiplayerShooterHUDWidget::Execute_UpdatePlayerGunAmmoCountVisibility(ShooterHUDWidget, InVisible);
	}
}

void AMultiplayerShooterHUD::UpdatePawn(APawn* InNewPawn)
{
	// Unbind from currently used pawn
	if (IsValid(ShooterCharacter))
	{
		if (InNewPawn != ShooterCharacter)
		{
			return;
		}
		ShooterCharacter->OnPlayerHealthChanged.RemoveDynamic(this, &AMultiplayerShooterHUD::HandlePlayerHealthChanged);
		ShooterCharacter->OnChangedGun.RemoveDynamic(this, &AMultiplayerShooterHUD::HandlePlayerChangedGun);
	}
	// Bind to new pawn
	ShooterCharacter = nullptr;
	if (IsValid(InNewPawn))
	{
		ShooterCharacter = Cast<AMultiplayerShooterCharacter>(InNewPawn);
		if (ShooterCharacter != nullptr)
		{
			HandlePlayerHealthChanged(ShooterCharacter->GetCurrentHealth());
			HandlePlayerChangedGun(ShooterCharacter->GetGun());
			ShooterCharacter->OnPlayerHealthChanged.AddDynamic(this, &AMultiplayerShooterHUD::HandlePlayerHealthChanged);
			ShooterCharacter->OnChangedGun.AddDynamic(this, &AMultiplayerShooterHUD::HandlePlayerChangedGun);
		}
	}
	if (ShooterCharacter == nullptr)
	{
		HandlePlayerHealthChanged(0);
		UpdatePlayerGunAmmoCountVisibility(false);
	}
}

void AMultiplayerShooterHUD::HandlePossessedPawnChanged(APawn*, APawn* InNewPawn)
{
	UpdatePawn(InNewPawn);
}

void AMultiplayerShooterHUD::HandlePlayerHealthChanged(const float InCurrentHealth)
{
	if (ShooterHUDWidget != nullptr)
	{
		IMultiplayerShooterHUDWidget::Execute_UpdatePlayerHealth(ShooterHUDWidget, InCurrentHealth);
	}
}

void AMultiplayerShooterHUD::HandleAmmoCountChanged(const int32 InCurrentAmmoCount)
{
	if (ShooterHUDWidget != nullptr)
	{
		IMultiplayerShooterHUDWidget::Execute_UpdatePlayerGunAmmoCount(ShooterHUDWidget, InCurrentAmmoCount);
	}
}

void AMultiplayerShooterHUD::HandlePlayerChangedGun(AGun* InNewGun)
{
	// Unbind from currently used gun
	if (IsValid(CurrentPlayerGun))
	{
		if (CurrentPlayerGun == InNewGun)
		{
			return;
		}
		CurrentPlayerGun->OnAmmoCountChanged.RemoveDynamic(this, &AMultiplayerShooterHUD::HandleAmmoCountChanged);
	}
	// Bind to new gun
	CurrentPlayerGun = InNewGun;
	const bool newGunValid = IsValid(InNewGun);
	UpdatePlayerGunAmmoCountVisibility(newGunValid);
	if (newGunValid)
	{
		HandleAmmoCountChanged(CurrentPlayerGun->GetCurrentAmmoCount());
		CurrentPlayerGun->OnAmmoCountChanged.AddDynamic(this, &AMultiplayerShooterHUD::HandleAmmoCountChanged);
	}
}