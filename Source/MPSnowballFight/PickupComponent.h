// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataAsset.h"
#include "Engine/TimerHandle.h"
#include "PickupComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MPSNOWBALLFIGHT_API UPickupComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPickupComponent();

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable, Category = "Pickup Item")
	void UsePickup(UObject* InContextObject);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Item")
	UDataAsset* PickupData = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Item")
	FVector2D PickupLifeTimeDurationRange = FVector2D(5.0f, 10.0f);	

private:
	void StartPickupLifeTimeTimer();

	UFUNCTION()
	void HandleLifeTimeTimerFinished();

	FTimerHandle PickupLifeTimeTimerHandle;
};
