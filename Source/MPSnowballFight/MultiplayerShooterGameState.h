// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Engine/TimerHandle.h"
#include "MultiplayerShooterGameState.generated.h"

/**
 * 
 */
UCLASS()
class MPSNOWBALLFIGHT_API AMultiplayerShooterGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Items")
	FVector2D NextPickupSpawnTimeRange = FVector2D(5.0f, 15.0f);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Items")
	TSet<TSubclassOf<AActor>> PickupActorClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup Items")
	FName PickupSpawnLocationActorTag = FName("Pickup");

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	void StartNextPickupSpawnTimer();
	void SpawnNewPickup();

	UFUNCTION()
	void HandleNextPickupSpawnTimerFinished();

	UFUNCTION()
	void HandlePickupActorDestroyed(AActor* InDestroyedActor);

	FTimerHandle NextPickupSpawnTimerHandle;
	TArray<AActor*> AvailablePickupSpawnLocations;
	TArray<TSubclassOf<AActor>> PickupActorClassesAsArray;
	TMap<AActor*, AActor*> SpawnedPickups;
};
