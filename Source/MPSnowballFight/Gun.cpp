// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	CurrentAmmoCount = FMath::Max(FMath::Min(InitialAmmoCount, MaxAmmoCount), 0);

	GunMesh = CreateDefaultSubobject<UStaticMeshComponent>("GunMesh");
	SetRootComponent(GunMesh);
	GunMesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
	GunMesh->SetCollisionProfileName("NoCollision");

	GunBarrelArrow = CreateDefaultSubobject<UArrowComponent>("GunBarrelArrow");
	GunBarrelArrow->SetupAttachment(GunMesh);
}

void AGun::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AGun, CooldownActive);
	DOREPLIFETIME(AGun, CurrentAmmoCount);
}

bool AGun::IsReadyToShoot() const
{
	return (!CooldownActive && (CurrentAmmoCount > 0) && (BulletClass != nullptr));
}

int32 AGun::GetCurrentAmmoCount() const
{
	return CurrentAmmoCount;
}

void AGun::TryShooting()
{
	if ((GetLocalRole() != ROLE_Authority) || !IsReadyToShoot())
	{
		return;
	}

	UpdateCurrentAmmoCount(CurrentAmmoCount - 1);

	FActorSpawnParameters bulletSpawnParameters;
	bulletSpawnParameters.Owner = this;
	bulletSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	GetWorld()->SpawnActor<AActor>(BulletClass, GunBarrelArrow->GetComponentLocation(), GunBarrelArrow->GetComponentRotation(), bulletSpawnParameters);
	if (CooldownDuration > 0.0f)
	{
		CooldownActive = true;
		FTimerHandle bulletLifeTimeTimerHandle;
		GetWorld()->GetTimerManager().SetTimer(bulletLifeTimeTimerHandle, this, &AGun::ResetCooldown, CooldownDuration);
	}
}

void AGun::TryAddingAmmo(const int32 InNewBulletsCount)
{
	if ((GetLocalRole() != ROLE_Authority) || (InNewBulletsCount <= 0))
	{
		return;
	}

	UpdateCurrentAmmoCount(FMath::Min(CurrentAmmoCount + InNewBulletsCount, MaxAmmoCount));
}

void AGun::UpdateCurrentAmmoCount(const int32 InNewAmmoCount)
{
	CurrentAmmoCount = InNewAmmoCount;
	OnRep_CurrentAmmoCount();
}

void AGun::ResetCooldown()
{
	CooldownActive = false;
}

void AGun::OnRep_CurrentAmmoCount()
{
	OnAmmoCountChanged.Broadcast(CurrentAmmoCount);
}
